//
//  NetworkUserLoginUserCase.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct NetworkUserLoginUserCase<UserLoginProviderType: Provider, ParserType: Parser>: UserLoginUserCase where UserLoginProviderType.ReturnType == (headers: [AnyHashable: Any], data: Data), ParserType.ParsableType == UserLoginResult {
    private let provider: UserLoginProviderType
    private let parser: ParserType
    private let credentialsKeeper: CredentialsKeeper
    
    init(provider: UserLoginProviderType, parser: ParserType, credentialsKeeper: CredentialsKeeper) {
        self.provider = provider
        self.parser = parser
        self.credentialsKeeper = credentialsKeeper
    }
    
    func login(user: User, completion: @escaping UserCaseActionCompletion<Error>) {
        provider.request(route: EnterprisesRoute.login(user: user)) { result in
            switch result {
            case .success(let headers, let data):
                do {
                    let userLoginResult = try self.parser.parse(data: data, toType: UserLoginResult.self)
                    if self.extractLoginCredentials(onHeaders: headers) {
                        completion(.success(userLoginResult.success))
                    } else {
                        completion(.failure(EnterprisesErrors.coulntExtractCredentials))
                    }
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                if let networkError = error as? NetworkError,
                   case let .httpError(statusCode) = networkError,
                   statusCode == 401 {
                    completion(.success(false))
                } else {
                    completion(.failure(error))
                }
            }
        }
    }
    
    func extractLoginCredentials(onHeaders headers: [AnyHashable : Any]) -> Bool {
        guard let accessToken = headers["access-token"] as? String,
              let uid = headers["uid"] as? String,
              let client = headers["client"] as? String else {
            return false
        }
        
        credentialsKeeper.keepInDisk(credentials: .init(token: accessToken, uid: uid, client: client))
        return true
    }
}
