//
//  NetworkLoggedUserUseCase.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct NetworkLoggedUserUseCase: LoggedUserUseCase {
    private let credentialsKeeper: CredentialsKeeper
    init(credentialsKeeper: CredentialsKeeper) {
        self.credentialsKeeper = credentialsKeeper
    }

    func getIsLoggedUser(completion: @escaping UserCaseCompletion<Bool, Error>) {
        if credentialsKeeper.readFromDisk() != nil {
            return completion(.success(true))
        } else {
            return completion(.success(false))
        }
    }
}
