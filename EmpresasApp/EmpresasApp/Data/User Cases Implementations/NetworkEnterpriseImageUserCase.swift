//
//  NetworkEnterpriseImageUserCase.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct NetworkEnterpriseImageUserCase<ImageProviderType: Provider>: EnterpriseImageUserCase where ImageProviderType.ReturnType == Data {
    private let imageProvider: ImageProviderType
    
    init(imageProvider: ImageProviderType) {
        self.imageProvider = imageProvider
    }
    
    func getImage(fromURL: URL, completion: @escaping UserCaseCompletion<Data, Error>) -> CancelCompletion {
        let cancellable = imageProvider.request(route: BasicRoute(url: fromURL), completion: completion)
        
        return cancellable?.cancel ?? { }
    }
}
