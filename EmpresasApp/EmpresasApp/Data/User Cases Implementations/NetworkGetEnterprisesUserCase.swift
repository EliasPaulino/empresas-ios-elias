//
//  NetworkGetEnterprisesUserCase.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct NetworkGetEnterprisesUserCase<ProviderType: ParserProvider>: GetEnterprisesUserCase where ProviderType.ParsableType == Page<Enterprise> {
    private let provider: ProviderType
    private let credentialsKeeper: CredentialsKeeper
    
    init(provider: ProviderType, credentialsKeeper: CredentialsKeeper) {
        self.provider = provider
        self.credentialsKeeper = credentialsKeeper
    }
    
    @discardableResult
    func getEnterprises(withType: EnterpriseType?, andQueryName name: String?, completion: @escaping UserCaseCompletion<[Enterprise], Error>) -> CancelCompletion {
        guard let credentials = credentialsKeeper.readFromDisk() else {
            completion(.failure(EnterprisesErrors.couldntLoadCredentials))
            return { }
        }
        
        let cancellable = self.provider.requestAndParse(route: EnterprisesRoute.enterprises(name: name, credentials: credentials)) { result in
            switch result {
            case .success(let enterprisesPage):
                completion(.success(enterprisesPage.elements))
            case .failure(let error):
                if let networkError = error as? NetworkError,
                   case let .httpError(statusCode) = networkError,
                   statusCode == 401 {
                    completion(.failure(DomainErrors.userCredentialsAreInvalid))
                }
                completion(.failure(error))
            }
        }
        return {
            cancellable?.cancel()
        }
    }
}
