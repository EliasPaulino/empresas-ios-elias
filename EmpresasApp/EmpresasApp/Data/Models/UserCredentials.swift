//
//  UserCredentials.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct UserCredentials {
    let token: String
    let uid: String
    let client: String
}
