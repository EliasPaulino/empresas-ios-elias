//
//  KeyChainCredentialsKeeper.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation
import KeychainAccess

struct KeyChainCredentialsKeeper: CredentialsKeeper {
    func keepInDisk(credentials: UserCredentials) {
        let keychain = Keychain(service: "com.zombiechallenge.EmpresasApp-tokens")
        keychain["client"] = credentials.client
        keychain["token"] = credentials.token
        keychain["uid"] = credentials.uid
    }
    
    func readFromDisk() -> UserCredentials? {
        let keychain = Keychain(service: "com.zombiechallenge.EmpresasApp-tokens")
        guard let client = keychain["client"],
              let token = keychain["token"],
              let uid = keychain["uid"] else {
            return nil
        }
        
        return UserCredentials(token: token, uid: uid, client: client)
    }
}
