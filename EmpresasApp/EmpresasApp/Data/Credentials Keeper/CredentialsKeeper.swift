//
//  CredentialsKeeper.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

protocol CredentialsKeeper {
    func keepInDisk(credentials: UserCredentials)
    func readFromDisk() -> UserCredentials?
}
