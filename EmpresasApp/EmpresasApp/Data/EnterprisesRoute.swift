//
//  EnterprisesRoute.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

enum EnterprisesRoute: Route {
    case login(user: User), enterprises(name: String?, credentials: UserCredentials)
    
    var baseURL: URL {
        return URL(string: "https://empresas.ioasys.com.br")!
    }
    
    var path: String {
        switch self {
        case .login:
            return "/api/v1/users/auth/sign_in"
        case .enterprises:
            return "/api/v1/enterprises"
        }
    }
    
    var method: HttpMethod {
        switch self {
        case .login:
            return .post
        case .enterprises:
            return .get
        }
    }
    
    var urlParams: [URLQueryItem] {
        switch self {
        case .login:
            return []
        case .enterprises(let name, _):
            return [.init(name: "name", value: name)]
        }
    }
    
    var headerParams: [String : String]? {
        switch self {
        case .login:
            return nil
        case .enterprises(_, let credentials):
            return [
                "client": credentials.client,
                "access-token": credentials.token,
                "uid": credentials.uid
            ]
        }
    }
    
    var bodyParams: BodyParams {
        switch self {
        case .login(let user):
            return .object(user)
        case .enterprises:
            return .nothing
        }
    }
}
