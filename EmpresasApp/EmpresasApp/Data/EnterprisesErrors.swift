//
//  EnterprisesErrors.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

enum EnterprisesErrors: Error {
    case coulntExtractCredentials, couldntLoadCredentials
}
