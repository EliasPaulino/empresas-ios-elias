//
//  UIView+UIViewController+loadable.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

extension UIView: LoadableView {
    @objc func loadingViewStrategy() -> LoadingInitViewStrategy {
        return DefaultLoadingInitViewStrategy()
    }
    var viewToLoad: UIView {
        return self
    }
}

extension UIViewController: LoadableView {
    @objc func loadingViewStrategy() -> LoadingInitViewStrategy {
        return DefaultLoadingInitViewStrategy()
    }
    var viewToLoad: UIView {
        return self.view
    }
}
