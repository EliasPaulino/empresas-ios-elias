//
//  NoBackgroundLoadingInitViewStrategy.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

@objc class NoBackgroundLoadingInitViewStrategy: NSObject, LoadingInitViewStrategy {
    
    lazy var loadingStyle: UIActivityIndicatorView.Style = {
        if #available(iOS 13.0, *) {
            return .large
        } else {
            return .whiteLarge
        }
    }()
    
    lazy var loadingView: LoadingView = UIActivityIndicatorView(style: loadingStyle).build {
        $0.startAnimating()
        $0.color = .color(ofType: .loginButtonPink)
        $0.setContentHuggingPriority(.defaultLow, for: .vertical)
        $0.setContentHuggingPriority(.defaultLow, for: .horizontal)
    }
}
