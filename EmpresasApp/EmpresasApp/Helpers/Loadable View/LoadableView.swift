//
//  LoadableView.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

/// A view that can show a loading indicator
protocol LoadableView {
    /// the view where the loading indicator is placed
    var viewToLoad: UIView { get }
    
    func loadingViewStrategy() -> LoadingInitViewStrategy
    
    func startLoading(fitToAnchorable: Anchorable?)
    func stopLoading()
}

extension LoadableView {
    var loadingView: UIView? {
        self.viewToLoad.subviews.filter { subView -> Bool in
            subView is LoadingView
        }.first
    }

    func startLoading(fitToAnchorable: Anchorable? = nil) {
        guard loadingView == nil else { return }
        
        let anchorableToFit = fitToAnchorable ?? viewToLoad.safeAreaLayoutGuide
        guard let loadingView = loadingViewStrategy().loadingView as? UIView else { return }
        
        viewToLoad.addSubview(loadingView)
        loadingView.layout.fill(view: anchorableToFit)
    }
    
    func stopLoading() {
        loadingView?.removeFromSuperview()
    }
}
