//
//  LoadingInitViewStrategy.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

/// A strategy for the loading indicator creation. The default implementation uses a simple
@objc protocol LoadingInitViewStrategy {
    var loadingView: LoadingView { get }
}

/// A protocol to be implemented by the loading indicator views
@objc protocol LoadingView {}

extension UIActivityIndicatorView: LoadingView {}
