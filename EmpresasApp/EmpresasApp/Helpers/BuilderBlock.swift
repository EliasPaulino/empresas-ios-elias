//
//  BuilderBlock.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

/// It defines a convenience method for making easy reusing setup closures.
public protocol BuilderBlock: AnyObject { }

public extension BuilderBlock {
    typealias SetupBlock = ((_ self: Self) -> Void)
    
    /// executes a block that builds any object and returns the set object
    /// - Parameter block: the block with the object changes
    @discardableResult
    func build(block: SetupBlock) -> Self {
        block(self)
        
        return self
    }
}

extension NSObject: BuilderBlock {}
