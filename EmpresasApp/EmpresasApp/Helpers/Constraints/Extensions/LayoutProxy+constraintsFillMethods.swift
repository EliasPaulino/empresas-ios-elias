//
//  LayoutProxy+constraintsFillMethods.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

extension LayoutPropertyProxy {
    /// it makes this Anchor property be equal to the superview
    public func equalToSuperView(margin: CGFloat = 0) {
        self.layout.makeRelation(to: nil, type: self.type, operation: .margin(margin))
    }
}
