//
//  LayoutPropertyProxy+constraintMethods.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

/// Any LayoutPropertyProxy has methods for handling constraints
extension LayoutPropertyProxy {
    // so, here we are only using the default UIKit constrait methods to make the LayoutPropertyProxy methods.
    @discardableResult
    public func equal(to otherAnchorLayout: DefaultLayoutPropertyProxy<AnchorType>, offsetBy constant: CGFloat = 0, priority: UILayoutPriority? = nil) -> NSLayoutConstraint {
        let constraint = anchor.constraint(equalTo: otherAnchorLayout.anchor, constant: constant)
        applyPriority(priority, to: constraint)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult
    public func greaterThanOrEqual(to otherAnchorLayout: DefaultLayoutPropertyProxy<AnchorType>, offsetBy constant: CGFloat = 0, priority: UILayoutPriority? = nil) -> NSLayoutConstraint {
        let constraint = anchor.constraint(greaterThanOrEqualTo: otherAnchorLayout.anchor, constant: constant)
        applyPriority(priority, to: constraint)
        constraint.isActive = true
        return constraint
    }
    
    @discardableResult
    public func lessThanOrEqual(to otherAnchorLayout: DefaultLayoutPropertyProxy<AnchorType>, offsetBy constant: CGFloat = 0) -> NSLayoutConstraint {
        let constraint = anchor.constraint(lessThanOrEqualTo: otherAnchorLayout.anchor, constant: constant)
        constraint.isActive = true
        return constraint
    }
    
    private func applyPriority(_ priority: UILayoutPriority?, to constraint: NSLayoutConstraint) {
        if let priority = priority {
            constraint.priority = priority
        }
    }
}
