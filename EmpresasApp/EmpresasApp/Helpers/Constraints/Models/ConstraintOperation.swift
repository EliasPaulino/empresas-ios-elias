//
//  ConstraintOperation.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

/// This are the two types of operation with constraint, multiply: for dimension constraints and margin for all of them.
public enum ConstraintOperation {
    case multiply(CGFloat), margin(CGFloat)
}
