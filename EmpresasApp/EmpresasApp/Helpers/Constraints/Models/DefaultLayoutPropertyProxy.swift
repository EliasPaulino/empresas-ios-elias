//
//  DefaultLayoutPropertyProxy.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

/// A type to make possible have references to a LayoutPropertyProxy.
public struct DefaultLayoutPropertyProxy<AnchorType: AnyObject>: LayoutPropertyProxy {
    public var type: LayoutPropertyProxyType
    public unowned var layout: LayoutProxy
    public var anchor: NSLayoutAnchor<AnchorType>
    
    public init(anchor: NSLayoutAnchor<AnchorType>, layout: LayoutProxy, type: LayoutPropertyProxyType) {
        self.layout = layout
        self.anchor = anchor
        self.type = type
    }
}
