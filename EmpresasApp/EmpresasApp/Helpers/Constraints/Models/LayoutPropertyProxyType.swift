//
//  LayoutPropertyProxyType.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

public enum LayoutPropertyProxyType {
    case top, bottom, left, right, centerX, centerY, width, height
}
