//
//  Route+initRouteType.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

public extension URLComponents {
    
    /// initilizes a URLComponents with a Route type
    init?(routeType: Route) {
        self.init(string: routeType.baseURL.absoluteString)
        self.path = routeType.path
        self.queryItems = routeType.urlParams
    }
}
