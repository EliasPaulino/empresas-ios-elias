//
//  Route+completeURL.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

public extension Route {
    /// This is a convenience variable for joining baseURL+path+urlParams
    var completeUrl: URL? {
        let urlComponents = URLComponents(routeType: self)
        return urlComponents?.url
    }

    var completeRequest: URLRequest? {
        guard let url = completeUrl else {
            return nil
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.allHTTPHeaderFields = self.headerParams
        return urlRequest
    }
}
