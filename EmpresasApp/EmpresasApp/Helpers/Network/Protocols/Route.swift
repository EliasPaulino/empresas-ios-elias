//
//  Route.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

/// A protocol for types that can make a Route.
/// Usually, each of the final applications has a Enum, and it has a case for each route.
public protocol Route {
    var baseURL: URL { get }
    var path: String { get }
    var method: HttpMethod { get }
    var urlParams: [URLQueryItem] { get }
    var headerParams: [String : String]? { get }
    var bodyParams: BodyParams { get }
}
