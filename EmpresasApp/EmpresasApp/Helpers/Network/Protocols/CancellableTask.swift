//
//  CancellableTask.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

/// A type that can be cancelled.
public protocol CancellableTask {
    func cancel()
}

extension URLSessionDataTask: CancellableTask { }

extension URLSessionDownloadTask: CancellableTask { }
