//
//  HttpMethod.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

//Its a enum representing the Http Methods Type.
public enum HttpMethod: String {
    case get, post, put, delete
}
