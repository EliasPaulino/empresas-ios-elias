//
//  BodyParams.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

public enum BodyParams {
    case data(Data)
    case object(Encodable)
    case nothing

    var data: Data? {
        switch self {
        case .nothing:
            return nil
        case .data(let data):
            return data
        case .object(let object):
            return object.data()
        }
    }
}

extension Encodable {
    func data() -> Data? {
        return try? JSONEncoder().encode(self)
    }
}
