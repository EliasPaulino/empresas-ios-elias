//
//  Page.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

private struct ElementsCodingKey: CodingKey {
    var stringValue: String = ""
    var intValue: Int? = nil

    init?(stringValue: String) {
        self.stringValue = stringValue
    }

    init?(intValue: Int) {
        self.intValue = intValue
    }
}

struct Wrapper<WrappedElement: Decodable>: Decodable {
    var element: WrappedElement
    
    init(from decoder: Decoder) throws {
        let jsonKeyForElements = Wrapper.jsonKeyForElement()
        let container = try decoder.container(keyedBy: ElementsCodingKey.self)
        self.element = try container.decode(WrappedElement.self, forKey: ElementsCodingKey(stringValue: jsonKeyForElements)!)
    }
    
    static func jsonKeyForElement() -> String {
        return "\(String(describing: WrappedElement.self).lowercased())"
    }
}

/// A page to decode JSON elements that arent on the JSON first level.
/// It uses the PageElement type  as the variable name to the items. Ex.: if the type is Article, the variable is "articles".
struct Page<PageElement: Codable>: Decodable {
    var elements: [PageElement]
    var keyName: String?
    
    init(from decoder: Decoder) throws {
        let jsonKeyForElements = keyName ?? "\(String(describing: PageElement.self).lowercased())s"

        let container = try decoder.container(keyedBy: ElementsCodingKey.self)
        self.elements = try container.decode([PageElement].self, forKey: ElementsCodingKey(stringValue: jsonKeyForElements)!)
    }
}
