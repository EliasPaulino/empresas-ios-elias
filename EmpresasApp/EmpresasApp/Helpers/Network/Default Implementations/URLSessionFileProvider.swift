//
//  URLSessionFileProvider.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

/// A provider implementation responsible for providing files. For example, this can be used for requesting images.
public class URLSessionFileProvider: FileProvider {
   
    /// Makes a request for the given Route
    /// - Parameters:
    ///   - route: the route containing the wanted data
    ///   - completion: a completion called when the request is completed. Returns the URL of the file or a error.
    @discardableResult
    public func request(route: Route, completion: @escaping (Result<Data, Error>) -> Void) -> CancellableTask? {
        let routeURL = route.baseURL
        let routeRequestURL = URLRequest(url: routeURL, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 60)
        
        let downloadTask = URLSession.shared.downloadTask(with: routeRequestURL) { (url, _, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let responseUrl = url,
                  let fileData = try? Data(contentsOf: responseUrl) else {
                completion(.failure(NetworkError.noResponseData))
                return
            }
            
            completion(.success(fileData))
        }
        
        downloadTask.resume()
        
        return downloadTask
    }
}
