//
//  URLSessionDataProvider.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

/// A default implementation of ParsableProvider made with URLSession and dataTask.
public class URLSessionParserProvider<ParserType: Parser>: ParserProvider {
    public var parser: ParserType
    
    private let urlSessionProvider = URLSessionProvider()
    
    /// Initilized the URLSessionProvider
    /// - Parameter parser: the parser that is going to be used for parsing the requested Data.
    public init(parser: ParserType) {
        self.parser = parser
    }
    
    @discardableResult
    public func request(route: Route, completion: @escaping (Result<Data, Error>) -> Void) -> CancellableTask? {

        return urlSessionProvider.request(route: route) { result in
            switch result {
            case .success(_, let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
