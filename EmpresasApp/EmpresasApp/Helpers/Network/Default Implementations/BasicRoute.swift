//
//  BasicRoute.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

public struct BasicRoute: Route {
    public var bodyParams: BodyParams = .nothing
    public var headerParams: [String : String]? = [:]
    public var baseURL: URL
    public var path: String = ""
    public var method: HttpMethod = .get
    public var urlParams: [URLQueryItem] = []
    
    
    public init(url: URL) {
        self.baseURL = url
    }
}
