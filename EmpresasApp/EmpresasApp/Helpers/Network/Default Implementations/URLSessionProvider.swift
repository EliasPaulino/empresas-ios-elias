//
//  URLSessionProvider.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct URLSessionProvider: Provider {
    typealias ReturnType = (headers: [AnyHashable: Any], data: Data)
    
    func request(route: Route, completion: @escaping (Result<(headers: [AnyHashable: Any], data: Data), Error>) -> Void) -> CancellableTask? {
        // the other route methods supossed to be implemmented when they be necessary in the future.
        guard route.method == .get || route.method == .post else {
            fatalError("URLSessionProvider doesnt support \(route.method.rawValue) method")
        }
        
        guard var routeRequestURL = route.completeRequest else {
            completion(.failure(NetworkError.wrongURL(route)))
            return nil
        }
        
        routeRequestURL.httpMethod = route.method.rawValue
        routeRequestURL.timeoutInterval = 60
        routeRequestURL.httpBody = route.bodyParams.data
        routeRequestURL.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let dataTask = URLSession.shared.dataTask(with: routeRequestURL) { (data, response, error) in
            if let error = error {
                let nsError = error as NSError
                ///if th error is caused by request Cancel, dont send it.
                guard nsError.code != -999 else { return }
                completion(.failure(error))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.noHttpResponse))
                return
            }
            switch httpResponse.statusCode {
            case 200...299:
                guard let responseData = data else {
                    completion(.failure(NetworkError.noResponseData))
                    return
                }
                completion(.success((httpResponse.allHeaderFields, responseData)))
            default:
                //only responses between 200 and 299 are normal responses.
                completion(.failure(NetworkError.httpError(httpResponse.statusCode)))
            }
        }
        
        dataTask.resume()
        
        return dataTask
    }
}
