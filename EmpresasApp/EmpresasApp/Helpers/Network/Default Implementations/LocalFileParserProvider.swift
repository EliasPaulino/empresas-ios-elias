//
//  LocalFileParserProvider.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

/// A provider that simulates requesting, while reading from a local file. It also can send intentional errors at the completion
public class LocalFileParserProvider<ParserType: Parser>: ParserProvider {
    public var parser: ParserType
    
    /// Initilized the provider
    /// - Parameters:
    ///   - parser: the parser to use with the provider dara result
    ///   - error: a error to be used when simulating intentional errors for unit testing.
    public init(parser: ParserType) {
        self.parser = parser
    }
    
    public func request(route: Route, completion: @escaping (Result<Data, Error>) -> Void) -> CancellableTask? {
        
        guard route.method == .get else {
            fatalError("MockProvider doesnt support \(route.method.rawValue) method yet")
        }
        
        let routeURL = route.baseURL
        
        do {
            let resultData = try Data(contentsOf: routeURL)
            completion(.success(resultData))
        } catch {
            completion(.failure(error))
        }
        
        return nil
    }
}
