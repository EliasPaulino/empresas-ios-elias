//
//  CacheFileProvider.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

//A provider that caches data that come from a FileProvider
class CacheFileProvider<FileProviderType: FileProvider>: FileProvider {
    private let cachableProvider: FileProviderType
    private let cacheStore = NSCache<NSString, NSData>()
    
    init(cachableProvider: FileProviderType) {
        self.cachableProvider = cachableProvider
    }

    func request(route: Route, completion: @escaping (Result<Data, Error>) -> Void) -> CancellableTask? {
        if let cachedData = cacheStore.object(forKey: route.baseURL.absoluteString as NSString) {
            completion(.success(cachedData as Data))
            return nil
        } else {
            let cancellableTask = self.cachableProvider.request(route: route) { [weak self] result in
                switch result {
                case .success(let data):
                    self?.cacheStore.setObject(data as NSData, forKey: route.baseURL.absoluteString as NSString)
                    completion(.success(data))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
            
            return cancellableTask
        }
    }
}
