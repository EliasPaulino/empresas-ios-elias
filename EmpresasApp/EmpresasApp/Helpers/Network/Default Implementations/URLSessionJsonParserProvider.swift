//
//  URLSessionJsonParserProvider.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

public class URLSessionJsonParserProvider<ParsableType: Decodable>: URLSessionParserProvider<JSONParser<ParsableType>> {
    public init(jsonDecoder: JSONDecoder? = nil) {
        guard let jsonDecoder = jsonDecoder else {
            super.init(parser: JSONParser())
            return
        }
        super.init(parser: JSONParser(jsonDecoder: jsonDecoder))
    }
}
