//
//  AppCompositionRoot.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class AppCompositionRoot {
    private let jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    private lazy var loginUserCase = NetworkUserLoginUserCase(
        provider: URLSessionProvider(),
        parser: JSONParser(jsonDecoder: jsonDecoder),
        credentialsKeeper: KeyChainCredentialsKeeper()
    )
    private lazy var getEnterprisesUserCase = NetworkGetEnterprisesUserCase(
        provider: URLSessionJsonParserProvider(jsonDecoder: jsonDecoder),
        credentialsKeeper: KeyChainCredentialsKeeper()
    )
    private lazy var homeViewController = HomeViewController(
        viewModel: .init(
            loggedUserCase: NetworkLoggedUserUseCase(credentialsKeeper: KeyChainCredentialsKeeper()),
            enterprisesUserCase: getEnterprisesUserCase,
            enterpriseViewModelInjector: {enterprise in
                .init(enterprise: enterprise,
                     imageUserCase: NetworkEnterpriseImageUserCase(
                        imageProvider: CacheFileProvider(
                            cachableProvider: URLSessionFileProvider()
                        )
                    )
                )
            },
            loginViewModelInjector: { [unowned self] _ in
                .init(loginUserCase: self.loginUserCase)
            }
        )
    )
    lazy var rootViewController = UINavigationController(rootViewController: homeViewController).build {
        $0.navigationBar.backgroundColor = .white
        $0.navigationBar.barTintColor = .white
        $0.navigationBar.isTranslucent = false
        $0.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                                                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .medium)]
        $0.navigationBar.shadowImage = UIImage()
    }
}
