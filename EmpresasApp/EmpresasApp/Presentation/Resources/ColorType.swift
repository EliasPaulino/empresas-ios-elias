//
//  ColorType.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

enum ColorType: String {
    case textFieldGray, loginButtonPink, textFieldTitleGray, loginWarningRed, homeCyan
}
