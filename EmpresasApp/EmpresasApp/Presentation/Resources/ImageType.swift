//
//  ImageType.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

enum ImageType: String {
    case launchBackground, loginBackground, loginLogo, showPassword, eraseTextField, homeHeader, searchIcon, navigationLeftIcon
}
