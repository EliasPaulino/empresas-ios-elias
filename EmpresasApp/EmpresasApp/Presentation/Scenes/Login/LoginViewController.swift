//
//  LoginViewController.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, ViewCodable, FailableViewController {
    private lazy var loginView = LoginView(viewModel: viewModel)
    private let viewModel: LoginViewModel
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func applyAditionalChanges() {
        modalPresentationStyle = .fullScreen
    }
    
    func observeViewModel() {
        viewModel.output.needFinishLoginFlow = { [weak self] in
            DispatchQueue.main.async {
                self?.dismiss(animated: true, completion: nil)
            }
        }
        viewModel.output.needShowError = { [weak self] title, message in
            DispatchQueue.main.async {
                self?.showError(title: title, message: message, closeActionTitle: "Fechar", firstAction: (title: "Tentar Novamente", completion: {
                    self?.viewModel.userTappedRetry()
                }))
            }
        }
    }
    
    override func loadView() {
        self.view = loginView
    }
}
