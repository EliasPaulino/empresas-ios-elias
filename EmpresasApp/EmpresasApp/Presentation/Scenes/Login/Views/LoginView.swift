//
//  LoginView.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class LoginView: UIView, ViewCodable {
    private lazy var emailTextField = TitledTextField(title: "Email", placeHolder: "email").build {
        $0.delegate = self
    }
    private lazy var passwordTextField = TitledTextField(title: "Senha", placeHolder: "Senha").build {
        $0.delegate = self
        $0.rightIconImage = .image(ofType: .showPassword)
        $0.isSecure = true
        $0.onRightButtonTap = showAndHidePasswordTapped
    }
    private lazy var enterButton = UIButton(type: .system).build {
        $0.setTitle("Enter".uppercased(), for: .normal)
        $0.tintColor = .white
        $0.backgroundColor = .color(ofType: .loginButtonPink)
        $0.contentEdgeInsets = .init(top: 15, left: 10, bottom: 15, right: 10)
        $0.layer.cornerRadius = 10
        $0.titleLabel?.font = .systemFont(ofSize: 16)
        $0.addTarget(self, action: #selector(enterButtonTapped), for: .touchUpInside)
    }
    private let headerImageView = LoginHeaderView().build {
        $0.image = UIImage.image(ofType: .loginBackground)
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }
    private let warningLabel = UILabel().build {
        $0.text = "Credenciais Incorretas"
        $0.textAlignment = .right
        $0.font = .systemFont(ofSize: 12)
        $0.textColor = .color(ofType: .loginWarningRed)
        $0.isHidden = true
    }
    private let loadingLayoutGuide = UILayoutGuide()
    private let movableContentLayoutGuideWithMargin = UILayoutGuide()
    private let movableContentLayoutGuide = UILayoutGuide()
    private var movableContentTopConstraint: NSLayoutConstraint?
    private var movableContentBottomConstraint: NSLayoutConstraint?
    private var keyBoardIsOpen = false
    private let viewModel: LoginViewModel

    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func enterButtonTapped() {
        self.endEditing(true)
        viewModel.userTappedEnter(withEmail: emailTextField.content, andPassword: passwordTextField.content)
    }
    
    private func showAndHidePasswordTapped(textField: TitledTextField) {
        passwordTextField.isSecure.toggle()
    }
    
    private func eraseTextFieldTapped(textField: TitledTextField) {
        textField.content = ""
        resetTextField(textField)
    }
    
    func applyAditionalChanges() {
        backgroundColor = .white
        bindkeyboardChanged()
    }
    
    func buildHierarchy() {
        addLayoutGuide(loadingLayoutGuide)
        addLayoutGuide(movableContentLayoutGuide)
        addLayoutGuide(movableContentLayoutGuideWithMargin)
        addSubviews(emailTextField, passwordTextField, enterButton, headerImageView, warningLabel)
    }
    
    func observeViewModel() {
        viewModel.output.needSetViewForValidCredentials = { [weak self] valid in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.passwordTextField.isSelected = !valid
                self.emailTextField.isSelected = !valid
                self.passwordTextField.rightIconImage = valid ? .image(ofType: .showPassword) :
                                                                .image(ofType: .eraseTextField)
                
                self.emailTextField.rightIconImage = valid ? nil : .image(ofType: .eraseTextField)
                self.passwordTextField.onRightButtonTap = valid ? self.showAndHidePasswordTapped : self.eraseTextFieldTapped
                self.emailTextField.onRightButtonTap = self.eraseTextFieldTapped
                self.warningLabel.isHidden = valid
            }
        }
        viewModel.output.needChangeLoadingState = { [weak self] hide in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if hide {
                    self.stopLoading()
                } else {
                    self.startLoading(fitToAnchorable: self.loadingLayoutGuide)
                }
            }
        }
        viewModel.output.needLoginData = { [weak self] in
            return (email: self?.emailTextField.content, password: self?.passwordTextField.content)
        }
    }
    
    func addConstraints() {
        var movableContentTopConstraint: NSLayoutConstraint?
        var movableContentBottomConstraint: NSLayoutConstraint?
        
        movableContentLayoutGuide.layout.build {
            movableContentTopConstraint = $0.top.equal(to: frameLayout.top)
            movableContentBottomConstraint = $0.bottom.lessThanOrEqual(to: frameLayout.bottom)
            $0.left.equal(to: frameLayout.left)
            $0.right.equal(to: frameLayout.right)
        }
        movableContentLayoutGuideWithMargin.layout.build {
            $0.top.equal(to: movableContentLayoutGuide.layout.top)
            $0.bottom.equal(to: movableContentLayoutGuide.layout.bottom)
            $0.left.equal(to: safeAreaLayoutGuide.layout.left, offsetBy: 16)
            $0.right.equal(to: safeAreaLayoutGuide.layout.right, offsetBy: -16)
        }
        headerImageView.layout.build {
            $0.group.left.top.right.fill(to: movableContentLayoutGuide)
            $0.height.equal(to: 270)
        }
        emailTextField.layout.build {
            $0.group.left.right.fill(to: movableContentLayoutGuideWithMargin)
            $0.top.equal(to: headerImageView.layout.bottom)
        }
        passwordTextField.layout.build {
            $0.group.left.right.fill(to: movableContentLayoutGuideWithMargin)
            $0.top.equal(to: emailTextField.layout.bottom, offsetBy: 16)
        }
        warningLabel.layout.build {
            $0.group.left.right.fill(to: movableContentLayoutGuideWithMargin)
            $0.top.equal(to: passwordTextField.layout.bottom, offsetBy: 4)
        }
        enterButton.layout.build {
            $0.group.left(10).bottom(-10).right(-10).fill(to: movableContentLayoutGuideWithMargin)
            $0.top.equal(to: warningLabel.layout.bottom, offsetBy: 20)
        }
        loadingLayoutGuide.layout.build {
            $0.top.equal(to: frameLayout.top)
            $0.bottom.equal(to: frameLayout.bottom)
            $0.right.equal(to: frameLayout.right)
            $0.left.equal(to: frameLayout.left)
        }
        self.movableContentTopConstraint = movableContentTopConstraint
        self.movableContentBottomConstraint = movableContentBottomConstraint
    }
}

// MARK: Keyboard Management
extension LoginView {
    private func bindkeyboardChanged() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func getKeyboardInformation(fromUserInfo userInfo: [AnyHashable : Any]?) -> (animationDuration: TimeInterval,
                                                                                         animationCurve: UIView.AnimationOptions,
                                                                                         frame: CGRect) {
        let keyBoardFrame = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect ?? .zero
        let keyBoardAnimationDuration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0.25
        let keyBoardAnimationOptionNumber = userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt ?? 1
        let keyBoardAnimationOption = UIView.AnimationOptions.init(rawValue: keyBoardAnimationOptionNumber)
        
        return (keyBoardAnimationDuration, keyBoardAnimationOption, keyBoardFrame)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        let (keyboardAnimationDuration, keyboardAnimationOption, keyboardFrame) = getKeyboardInformation(fromUserInfo: notification.userInfo)
        let necessaryYMovement = movableContentLayoutGuide.layoutFrame.maxY - keyboardFrame.origin.y
        
        guard necessaryYMovement > 0, !keyBoardIsOpen else { return }
        keyBoardIsOpen = true
        
        let keyBoardOnlyYMovement = keyboardFrame.height - necessaryYMovement
        let movableContentMovementPercentage = TimeInterval(keyBoardOnlyYMovement/keyboardFrame.height)
        let movableContentDelayAnimation = keyboardAnimationDuration * movableContentMovementPercentage
        let movableContenAnimationDuration = keyboardAnimationDuration - movableContentDelayAnimation
        self.movableContentTopConstraint?.constant = -necessaryYMovement
        self.movableContentBottomConstraint?.constant = -necessaryYMovement
        self.headerImageView.hideTitle()
        
        UIView.animate(withDuration: movableContenAnimationDuration, delay: movableContentDelayAnimation, options: keyboardAnimationOption, animations: {
            self.layoutIfNeeded()
        })
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        let (keyboardAnimationDuration, _, _) = getKeyboardInformation(fromUserInfo: notification.userInfo)
        guard keyBoardIsOpen else { return }
        keyBoardIsOpen = false
        self.movableContentTopConstraint?.constant = 0
        self.movableContentBottomConstraint?.constant = 0
        self.headerImageView.showTitle()
        
        UIView.animate(withDuration: keyboardAnimationDuration) {
            self.layoutIfNeeded()
        }
    }
    
    func resetTextField(_ textField: TitledTextField) {
        textField.isSelected = false
        if textField == emailTextField {
            textField.rightIconImage = nil
        } else {
            textField.rightIconImage = .image(ofType: .showPassword)
            textField.onRightButtonTap = showAndHidePasswordTapped
        }
    }
}

extension LoginView: TitledTextFieldDelegate {
    func textFieldShouldReturn(_ textField: TitledTextField) -> Bool {
        self.endEditing(true)
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: TitledTextField) {
        resetTextField(textField)
    }
}
