//
//  TitledTextField.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

protocol TitledTextFieldDelegate: AnyObject {
    func textFieldShouldReturn(_ textField: TitledTextField) -> Bool
    func textFieldDidBeginEditing(_ textField: TitledTextField)
}

class TitledTextField: UIView, ViewCodable {
    private lazy var textField = TextFieldWithPadding(padding: .init(top: 15, left: 10, bottom: 15, right: 10)).build {
        $0.backgroundColor = .color(ofType: .textFieldGray)
        $0.layer.cornerRadius = 5
        $0.delegate = self
        $0.layer.borderColor = UIColor.color(ofType: .loginWarningRed).cgColor
        $0.rightView = leftIconImageView
        $0.rightViewMode = UITextField.ViewMode.always
        $0.textColor = .color(ofType: .textFieldTitleGray)
    }
    private lazy var titleLabel = UILabel().build {
        $0.numberOfLines = 0
        $0.textColor = .color(ofType: .textFieldTitleGray)
        $0.font = .systemFont(ofSize: 14)
    }
    private lazy var leftIconImageView = UIButton(type: .system).build {
        $0.contentMode = .scaleAspectFit
        $0.addTarget(self, action: #selector(rightButtonTapped), for: UIControl.Event.touchUpInside)
    }
    
    weak var delegate: TitledTextFieldDelegate?
    
    var onRightButtonTap: ((TitledTextField) -> Void)?
    
    var isSecure: Bool {
        set {
            textField.isSecureTextEntry = newValue
        } get {
            return textField.isSecureTextEntry
        }
    }
    
    var rightIconImage: UIImage? {
        set {
            leftIconImageView.setImage(newValue?.withRenderingMode(.alwaysOriginal), for: .normal)
        } get {
            return leftIconImageView.currentImage
        }
    }
    
    var title: String? {
        set {
            titleLabel.text = newValue
        } get {
            return titleLabel.text
        }
    }
    
    var content: String? {
        set {
            textField.text = newValue
        } get {
            return textField.text
        }
    }
    
    var contentType: UITextContentType {
        set {
            textField.textContentType = newValue
        } get {
            return textField.textContentType
        }
    }
    
    var placeHolder: String? {
        set {
            textField.placeholder = newValue
        } get {
            return textField.placeholder
        }
    }
    
    var isSelected: Bool {
        set {
            self.textField.layer.borderWidth = newValue ? 1 : 0
            self.titleLabel.textColor = newValue ? .color(ofType: .loginWarningRed) :
                                                   .color(ofType: .textFieldTitleGray)
        } get {
            return self.textField.layer.borderWidth == 1
        }
    }
    
    init(title: String? = nil, placeHolder: String? = nil, content: String? = nil) {
        super.init(frame: .zero)
        self.titleLabel.text = title
        self.textField.text = content
        self.textField.placeholder = placeHolder
        
        self.content = content
        self.title = title
        self.placeHolder = placeHolder
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildHierarchy() {
        addSubviews(titleLabel, textField)
    }
    
    func addConstraints() {
        titleLabel.layout.group.top.left.right.fillToSuperView()
        textField.layout.build {
            $0.group.bottom.left.right.fillToSuperView()
            $0.top.equal(to: titleLabel.layout.bottom, offsetBy: 4)
        }
    }
    
    @objc func rightButtonTapped() {
        onRightButtonTap?(self)
    }
}

extension TitledTextField: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldReturn(self) ?? true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textFieldDidBeginEditing(self)
    }
}
