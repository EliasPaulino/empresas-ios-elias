//
//  PaddingWithMargin.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class TextFieldWithPadding: UITextField {
    let padding: UIEdgeInsets
    
    init(padding: UIEdgeInsets) {
        self.padding = padding
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var rightViewFrame = bounds.inset(by: padding)
        rightViewFrame.size.width = rightViewFrame.height
        rightViewFrame.origin.x = bounds.maxX - rightViewFrame.height - padding.right
        
        return rightViewFrame
    }
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var leftViewFrame = bounds.inset(by: padding)
        leftViewFrame.size.width = leftViewFrame.height
        leftViewFrame.origin.x = 0 + padding.left
        
        return leftViewFrame
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return calculateContentFrame(forBounds: bounds)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return calculateContentFrame(forBounds: bounds)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return calculateContentFrame(forBounds: bounds)
    }
    
    private func calculateContentFrame(forBounds bounds: CGRect) -> CGRect {
        var contentFrame = bounds.inset(by: padding)
        if leftView != nil {
            contentFrame.origin.x += bounds.height
            contentFrame.size.width -= bounds.height
        }
        if rightView != nil {
            contentFrame.size.width -= bounds.height
        }
        return contentFrame
    }
}
