//
//  LoginHeaderView.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class LoginHeaderView: UIImageView, ViewCodable {
    private var maskShapeLayer: CAShapeLayer {
        CAShapeLayer().build {
            $0.path = UIBezierPath().build {
                $0.move(to: bounds.origin)
                $0.addLine(to: CGPoint(x: bounds.maxX, y: 0))
                $0.addLine(to: CGPoint(x: bounds.maxX, y: bounds.height * 0.75))
                $0.addQuadCurve(to: CGPoint(x: 0, y: bounds.height * 0.75),
                                controlPoint: CGPoint(x: bounds.width/2, y: bounds.maxY))
                $0.addLine(to: bounds.origin)
            }.cgPath
        }
    }
    private let titleLabel = UILabel().build {
        $0.text = "Seja bem vindo ao empresas"
        $0.textAlignment = .center
        $0.font = .systemFont(ofSize: 20)
        $0.textColor = .white
    }
    private let titleLogoImageView = UIImageView().build {
        $0.image = .image(ofType: .loginLogo)
        $0.contentMode = .scaleAspectFit
    }
    
    private var titleBottoConstraint: NSLayoutConstraint?
    
    init() {
        super.init(frame: .zero)
        setupView()
    }
    
    func buildHierarchy() {
        addSubviews(titleLabel, titleLogoImageView)
    }
    
    func addConstraints() {
        var titleBottoConstraint: NSLayoutConstraint?
        titleLogoImageView.layout.build {
            $0.group.centerX.fill(to: safeAreaLayoutGuide)
            $0.width.equal(to: 40)
            $0.height.equal(to: $0.width)
        }
        titleLabel.layout.build {
            $0.group.left.right.fill(to: safeAreaLayoutGuide)
            titleBottoConstraint = $0.bottom.equal(to: safeAreaLayoutGuide.layout.bottom, offsetBy: -100)
            $0.top.equal(to: titleLogoImageView.layout.bottom, offsetBy: 16)
        }
        self.titleBottoConstraint = titleBottoConstraint
    }
    
    func hideTitle() {
        titleBottoConstraint?.constant = -20
         UIView.animate(withDuration: 0.2, delay: 0.2, options: .curveEaseIn, animations: {
            self.titleLabel.layer.opacity = 0
            self.layoutIfNeeded()
        })
    }
    
    func showTitle() {
        titleBottoConstraint?.constant = -100
        UIView.animate(withDuration: 0.2, delay: 0.2, options: .curveEaseIn, animations: {
            self.titleLabel.layer.opacity = 1
            self.layoutIfNeeded()
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.mask = maskShapeLayer
    }
}
