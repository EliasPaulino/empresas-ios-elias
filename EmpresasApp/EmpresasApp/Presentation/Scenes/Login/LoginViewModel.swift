//
//  LoginViewModel.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct LoginViewModelOutput {
    var needSetViewForValidCredentials: ((_ valid: Bool) -> Void)?
    var needFinishLoginFlow: (() -> Void)?
    var needChangeLoadingState: ((_ hide: Bool) -> Void)?
    var needShowError: ((_ title: String, _ message: String) -> Void)?
    var needLoginData: (() -> (email: String?, password: String?))?
}

typealias Injector<DataType, ViewModelType> = (DataType) -> ViewModelType

class LoginViewModel {
    var output: LoginViewModelOutput = .init()
    
    private let loginUserCase: UserLoginUserCase
    
    init(loginUserCase: UserLoginUserCase) {
        self.loginUserCase = loginUserCase
    }
    
    func userTappedRetry() {
        guard let loginData = output.needLoginData?() else { return }
        userTappedEnter(withEmail: loginData.email, andPassword: loginData.password)
    }
    
    func userTappedEnter(withEmail email: String?, andPassword password: String?) {
        guard let email = email, let password = password, !email.isEmpty, !password.isEmpty else {
            output.needSetViewForValidCredentials?(false)
            return
        }
        output.needChangeLoadingState?(false)
        loginUserCase.login(user: User(email: email, password: password)) { [weak self] result in
            self?.output.needChangeLoadingState?(true)
            switch result {
            case .success(let loginDidHappen):
                if loginDidHappen {
                    self?.output.needFinishLoginFlow?()
                } else {
                    self?.output.needSetViewForValidCredentials?(false)
                }
            case .failure:
                self?.output.needShowError?("Error", "Aconteceu Um Erro, por favor tente novamente.")
            }
        }
    }
}
