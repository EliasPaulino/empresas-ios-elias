//
//  EnterpriseView.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class EnterpriseView: UIView, ViewCodable {
    private let enterpriseImageView = UIImageView().build {
        $0.backgroundColor = .color(ofType: .homeCyan)
    }
    private lazy var descriptionTextView = UITextView().build {
        $0.text = viewModel.enterpriseDescription
        $0.font = .systemFont(ofSize: 18)
        $0.backgroundColor = .white
        $0.textColor = .black
    }
    private var viewModel: EnterpriseViewModel
    
    init(viewModel: EnterpriseViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func applyAditionalChanges() {
        backgroundColor = .white
    }
    
    func observeViewModel() {
        viewModel.output.changeLoadingViewState = { [weak self] hiding in
            DispatchQueue.main.async {
                if hiding {
                    self?.enterpriseImageView.startAnimating()
                } else {
                    self?.enterpriseImageView.stopAnimating()
                }
            }
        }
        viewModel.output.needReloadImage = { [weak self] imageData in
            DispatchQueue.main.async {
                self?.enterpriseImageView.image = UIImage(data: imageData)
            }
        }
        
        viewModel.viewIsSet()
    }
    
    func buildHierarchy() {
        addSubviews(enterpriseImageView, descriptionTextView)
    }
    
    func addConstraints() {
        enterpriseImageView.layout.build {
            $0.top.equal(to: safeAreaLayoutGuide.layout.top)
            $0.left.equal(to: frameLayout.left)
            $0.right.equal(to: frameLayout.right)
            $0.height.equal(to: 120)
        }
        descriptionTextView.layout.build {
            $0.group.bottom.left(8).right(-8).fill(to: safeAreaLayoutGuide)
            $0.top.equal(to: enterpriseImageView.layout.bottom, offsetBy: 24)
        }
    }
}
