//
//  EnterpriseViewController.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class EnterpriseViewController: UIViewController, ViewCodable {
    private lazy var enterpriseView = EnterpriseView(viewModel: viewModel)
    private let viewModel: EnterpriseViewModel
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    init(viewModel: EnterpriseViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = enterpriseView
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func applyAditionalChanges() {
        title = viewModel.enterpriseTitle
        let leftIconImage = UIImage.image(ofType: .navigationLeftIcon).withRenderingMode(.alwaysOriginal)
        navigationItem.leftBarButtonItem = .init(image: leftIconImage, style: .plain, target: self, action: #selector(popViewController))
    }
    
    @objc func popViewController() {
        navigationController?.popViewController(animated: true)
    }
}
