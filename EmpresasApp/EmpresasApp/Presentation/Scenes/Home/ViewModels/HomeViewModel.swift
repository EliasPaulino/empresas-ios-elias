//
//  HomeViewModel.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct HomeViewModelOutput {
    var needBeginLoginFlow: ((_ viewModel: LoginViewModel) -> Void)?
    var needReloadEnterprises: (() -> Void)?
    var needReloadResultView: ((String) -> Void)?
    var changeLoadingViewState: ((_ hiding: Bool) -> Void)?
    var needChangeToNextFlow: ((_ viewModel: EnterpriseViewModel) -> Void)?
    var needShowError: ((_ title: String, _ message: String) -> Void)?
}

class HomeViewModel {
    var output: HomeViewModelOutput = .init()
    
    var numberOfEnterprises: Int {
        return self.enterprises.count
    }
    
    private let loggedUserCase: LoggedUserUseCase
    private let enterprisesUserCase: GetEnterprisesUserCase
    private let loginViewModelInjector: Injector<Void, LoginViewModel>
    private let enterpriseViewModelInjector: Injector<Enterprise, EnterpriseViewModel>
    private var enterprises: [Enterprise] = []
    private var cancelGetCompletion: CancelCompletion?
    
    init(loggedUserCase: LoggedUserUseCase,
         enterprisesUserCase: GetEnterprisesUserCase,
         enterpriseViewModelInjector: @escaping Injector<Enterprise, EnterpriseViewModel>,
         loginViewModelInjector: @escaping Injector<Void, LoginViewModel>) {
        
        self.loggedUserCase = loggedUserCase
        self.enterprisesUserCase = enterprisesUserCase
        self.loginViewModelInjector = loginViewModelInjector
        self.enterpriseViewModelInjector = enterpriseViewModelInjector
    }
    
    func viewIsSet() {
        getData(queryName: nil)
    }
    
    private func getEnterprises(queryName: String?) {
        output.changeLoadingViewState?(false)
        self.cancelGetCompletion = enterprisesUserCase.getEnterprises(withType: nil, andQueryName: queryName) { [weak self] result in
            guard let self = self else { return }
            self.output.changeLoadingViewState?(true)
            switch result {
            case .success(let enterprises):
                self.enterprises = enterprises
                self.output.needReloadEnterprises?()
                self.output.needReloadResultView?("\(enterprises.count) resultados encontrados")
            case .failure(let error):
                if let error = error as? DomainErrors, error == .userCredentialsAreInvalid{
                    self.output.needBeginLoginFlow?(self.loginViewModelInjector(()))
                    return
                } 
                self.output.needShowError?("Error", "Aconteceu Um Erro, por favor tente novamente.")
            }
        }
    }
    
    private func getData(queryName: String?) {
        output.changeLoadingViewState?(false)
        loggedUserCase.getIsLoggedUser { [weak self] result in
            guard let self = self else { return }
            self.output.changeLoadingViewState?(true)
            switch result {
            case .success(let userIsLogged):
                if userIsLogged {
                    self.getEnterprises(queryName: queryName)
                } else{
                    self.output.needBeginLoginFlow?(self.loginViewModelInjector(()))
                }
            case .failure:
                self.output.needShowError?("Error", "Aconteceu Um Erro, por favor tente novamente.")
            }
        }
    }
    
    func userTappedRetry() {
        getData(queryName: nil)
    }
    
    func enterpriseViewModel(forPosition position: Int) -> EnterpriseViewModel {
        return enterpriseViewModelInjector(enterprises[position])
    }
    
    func userTappedNewEnterpriseName(_ name: String?) {
        self.cancelGetCompletion?()
        self.enterprises = []
        self.output.needReloadEnterprises?()
        guard let name = name, !name.isEmpty else {
            getData(queryName: nil)
            return
        }
        getData(queryName: name)
    }
    
    func userDidSelectEnterprise(atPositon position: Int) {
        output.needChangeToNextFlow?(enterpriseViewModelInjector(enterprises[position]))
    }
}
