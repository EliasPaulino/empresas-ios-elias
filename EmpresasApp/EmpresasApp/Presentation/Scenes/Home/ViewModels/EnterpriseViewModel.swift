//
//  EnterpriseViewModel.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct EnterpriseViewModelOutput {
    var needReloadImage: ((Data) -> Void)?
    var changeLoadingViewState: ((_ hiding: Bool) -> Void)?
}

class EnterpriseViewModel {
    var output: EnterpriseViewModelOutput = .init()
    private let enterprise: Enterprise
    private let imageUserCase: EnterpriseImageUserCase
    private var cancelCompletion: CancelCompletion?
    
    var enterpriseDescription: String {
        return enterprise.description
    }
    var enterpriseTitle: String {
        return enterprise.enterpriseName
    }
    init(enterprise: Enterprise, imageUserCase: EnterpriseImageUserCase) {
        self.enterprise = enterprise
        self.imageUserCase = imageUserCase
    }
    
    func viewIsSet() {
        getImage()
    }
    
    private func getImage() {
        guard let imageUrl = enterprise.photo else { return }

        output.changeLoadingViewState?(false)
        cancelCompletion = imageUserCase.getImage(fromURL: imageUrl) { result in
            switch result {
            case .success(let imageData):
                self.output.needReloadImage?(imageData)
            case .failure:
                self.output.needReloadImage?(Data())
            }
            self.output.changeLoadingViewState?(true)
        }
    }
    
    func viewWillBeReused() {
        self.output.needReloadImage?(Data())
    }
    
    deinit {
        cancelCompletion?()
    }
}
