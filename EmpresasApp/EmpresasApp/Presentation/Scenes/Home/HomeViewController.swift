//
//  HomeViewController.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, ViewCodable, FailableViewController {
    private let viewModel: HomeViewModel
    private lazy var homeView = HomeView(viewModel: viewModel)
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = homeView
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.viewIsSet()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func observeViewModel() {
        viewModel.output.needBeginLoginFlow = { [weak self] loginViewModel in
            DispatchQueue.main.async {
                self?.present(LoginViewController(viewModel: loginViewModel), animated: true)
            }
        }
        viewModel.output.needChangeToNextFlow = { [weak self] viewModel in
            DispatchQueue.main.async {
                self?.navigationController?.pushViewController(EnterpriseViewController(viewModel: viewModel), animated: true)
            }
        }
        viewModel.output.needShowError = { [weak self] title, message in
            DispatchQueue.main.async {
                self?.showError(title: title, message: message, closeActionTitle: "Fechar", firstAction: (title: "Tentar Novamente", completion: {
                    self?.viewModel.userTappedRetry()
                }))
            }
        }
    }
}
