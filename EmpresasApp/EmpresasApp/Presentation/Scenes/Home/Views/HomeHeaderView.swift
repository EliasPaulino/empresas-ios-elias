//
//  HomeHeaderView.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class HomeHeaderView: UIView, ViewCodable {
    private let headerImageView = UIImageView().build {
        $0.image = .image(ofType: .homeHeader)
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }
    private let searchResultsLabel = UILabel().build {
        $0.font = .systemFont(ofSize: 14)
        $0.textColor = .color(ofType: .textFieldTitleGray)
    }
    private lazy var textField = TextFieldWithPadding(padding: .init(top: 12, left: 12, bottom: 12, right: 12)).build {
        $0.placeholder = "Pesquise por empresa"
        $0.backgroundColor = .color(ofType: .textFieldGray)
        $0.textColor = .color(ofType: .textFieldTitleGray)
        $0.layer.cornerRadius = 5
        $0.font = .systemFont(ofSize: 14)
        $0.leftView = UIImageView(image: .image(ofType: .searchIcon))
        $0.leftView?.isUserInteractionEnabled = true
        $0.leftView?.frame.size = .init(width: 20, height: 20)
        $0.leftViewMode = .always
        $0.font = .systemFont(ofSize: 18)
        $0.delegate = self
        $0.addTarget(self, action: #selector(textFieldChanged), for: UIControl.Event.editingChanged)
    }
    
    var onUserWriteNewText: ((String?) -> Void)?
    
    var fieldIsDisabled: Bool {
        set {
            textField.isEnabled = newValue
        } get {
            textField.isEnabled 
        }
    }
    
    var resultsContent: String? {
        set {
            searchResultsLabel.text = newValue
        } get {
            return searchResultsLabel.text
        }
    }
    
    init() {
        super.init(frame: .zero)
        setupView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildHierarchy() {
        addSubviews(headerImageView, textField, searchResultsLabel)
    }
    
    func addConstraints() {
        textField.layout.build {
            $0.height.equal(to: 48)
            $0.group.left(16).right(-16).fillToSuperView()
        }
        searchResultsLabel.layout.build {
            $0.group.left(16).bottom(-16).right(-16).fillToSuperView()
            $0.top.equal(to: textField.layout.bottom, offsetBy: 16)
        }
        headerImageView.layout.build {
            $0.group.top.bottom(-71).left.right.fillToSuperView()
        }
    }
    
    func resetSearchContent() {
        textField.text = ""
    }
    
    @objc func textFieldChanged() {
        onUserWriteNewText?(textField.text)
    }
}

extension HomeHeaderView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
}
