//
//  EnterpriseTableCell.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class EnterpriseTableCell: UITableViewCell, ViewCodable {
    private let enterpriseImageView = UIImageView().build {
        $0.backgroundColor = .color(ofType: .homeCyan)
        $0.layer.cornerRadius = 5
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }
    private var viewModel: EnterpriseViewModel?
    private let loadingLayoutGuide = UILayoutGuide()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setViewModel(_ viewModel: EnterpriseViewModel) {
        self.viewModel?.viewWillBeReused()
        self.viewModel = viewModel
        self.viewModel?.output.changeLoadingViewState = { [weak self] hiding in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if hiding {
                    self.stopLoading()
                } else {
                    self.startLoading(fitToAnchorable: self.loadingLayoutGuide)
                }
            }
        }
        self.viewModel?.output.needReloadImage = { [weak self] imageData in
            DispatchQueue.main.async {
                self?.enterpriseImageView.image = UIImage(data: imageData)
            }
        }
        
        self.viewModel?.viewIsSet()
    }
    
    func applyAditionalChanges() {
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    func buildHierarchy() {
        addLayoutGuide(loadingLayoutGuide)
        addSubviews(enterpriseImageView)
    }
    
    func addConstraints() {
        loadingLayoutGuide.layout.fill(view: enterpriseImageView)
        enterpriseImageView.layout.fill(view: safeAreaLayoutGuide, margin: 8)
        enterpriseImageView.layout.height.equal(to: 120)
    }
    
    override func loadingViewStrategy() -> LoadingInitViewStrategy {
        return NoBackgroundLoadingInitViewStrategy()
    }
}
