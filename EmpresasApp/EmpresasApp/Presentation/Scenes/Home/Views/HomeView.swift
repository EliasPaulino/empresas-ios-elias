//
//  HomeView.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

class HomeView: UIView, ViewCodable {
    private let viewModel: HomeViewModel
    private lazy var enterprisesTableView = UITableView(frame: .zero, style: .grouped).build {
        $0.dataSource = self
        $0.delegate = self
        $0.estimatedRowHeight = UITableView.automaticDimension
        $0.registerReusableCell(forCellType: EnterpriseTableCell.self)
        $0.separatorStyle = .none
        $0.backgroundColor = .clear
        $0.contentInset = .init(top: Constants.tableViewTopMargin, left: 0, bottom: 0, right: 0)
    }
    private lazy var headerView = HomeHeaderView().build {
        $0.backgroundColor = .white
        $0.onUserWriteNewText = { [weak self] text in
            self?.viewModel.userTappedNewEnterpriseName(text)
        }
    }
    private let loadingViewGuide = UILayoutGuide()
    private var headerBottomConstraint: NSLayoutConstraint?
    private var tableViewBottomConstraint: NSLayoutConstraint?
            
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        setupView()
    }
    
    func applyAditionalChanges() {
        backgroundColor = .white
        bindkeyboardChanged()
    }
    
    func buildHierarchy() {
        addLayoutGuide(loadingViewGuide)
        addSubviews(enterprisesTableView, headerView)
    }
    
    func addConstraints() {
        headerView.layout.build {
            $0.top.equal(to: frameLayout.top)
            $0.left.equal(to: frameLayout.left)
            $0.right.equal(to: frameLayout.right)
            headerBottomConstraint = $0.bottom.equal(to: $0.top, offsetBy: 78)
        }
        enterprisesTableView.layout.build {
            $0.group.left(8).top.right(-8).fill(to: safeAreaLayoutGuide)
            tableViewBottomConstraint = $0.bottom.equal(to: safeAreaLayoutGuide.layout.bottom)
        }
        loadingViewGuide.layout.build {
            $0.top.equal(to: headerView.layout.bottom)
            $0.left.equal(to: frameLayout.left)
            $0.right.equal(to: frameLayout.right)
            $0.bottom.equal(to:  enterprisesTableView.layout.bottom)
        }
    }
    
    func observeViewModel() {
        viewModel.output.needReloadEnterprises = { [weak self] in
            DispatchQueue.main.async {
                self?.enterprisesTableView.reloadData()
                self?.enterprisesTableView.setContentOffset(.init(x: 0, y: -Constants.headerMinHeight), animated: false)
            }
        }
        viewModel.output.needReloadResultView = { [weak self] resultText in
            DispatchQueue.main.async {
                self?.headerView.resultsContent = resultText
            }
        }
        viewModel.output.changeLoadingViewState = { [weak self] hiding in
            guard let self = self else { return }
            DispatchQueue.main.async {
                if hiding {
                    self.stopLoading()
                } else {
                    self.startLoading(fitToAnchorable: self.loadingViewGuide)
                }
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadingViewStrategy() -> LoadingInitViewStrategy {
        return NoBackgroundLoadingInitViewStrategy()
    }
}


// MARK: Keyboard Management
extension HomeView {
    private func bindkeyboardChanged() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillAppear(notification: NSNotification) {
        guard  let keyBoardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
        tableViewBottomConstraint?.constant = -keyBoardFrame.height
        enterprisesTableView.setContentOffset(.init(x: 0, y: -Constants.headerMinHeight), animated: true)
        
        UIView.animate(withDuration: 0.25) {
            self.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardWillDisappear(notification: NSNotification) {
        tableViewBottomConstraint?.constant = 0
    }
}
extension HomeView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfEnterprises
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(forCellType: EnterpriseTableCell.self, for: indexPath).build {
            $0.setViewModel(viewModel.enterpriseViewModel(forPosition: indexPath.row))
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.endEditing(true)
        self.headerView.resetSearchContent()
        viewModel.userDidSelectEnterprise(atPositon: indexPath.row)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollOffset = -scrollView.contentOffset.y + Constants.tableViewTopMargin/3
        if scrollOffset > Constants.headerMinHeight + Constants.tableViewTopMargin/5 {
             headerBottomConstraint?.constant = scrollOffset
        }
    }
}

private struct Constants {
    static let tableViewTopMargin: CGFloat = 170
    static let headerMinHeight: CGFloat = 95
}
