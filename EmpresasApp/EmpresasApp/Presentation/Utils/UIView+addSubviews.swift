//
//  UIView+addSubviews.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

extension UIView {
    func addSubviews(_ subviews: [UIView]) {
        subviews.forEach { (subview) in
            self.addSubview(subview)
        }
    }
    
    func addSubviews(_ subviews: UIView...) {
        subviews.forEach { (subview) in
            self.addSubview(subview)
        }
    }
}
