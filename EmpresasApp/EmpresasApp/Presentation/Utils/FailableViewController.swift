//
//  FailableViewController.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

protocol FailableViewController: UIViewController {
    func showError(title: String, message: String, closeActionTitle: String, firstAction: (title: String, completion: () -> Void)?)
}

extension FailableViewController {
    func showError(title: String, message: String, closeActionTitle: String, firstAction: (title: String, completion: () -> Void)?) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let firstActionTitle = firstAction?.title {
            alertViewController.addAction(.init(title: firstActionTitle, style: .default, handler: { (_) in
                firstAction?.completion()
            }))
        }
        
        alertViewController.addAction(.init(title: closeActionTitle, style: .destructive, handler: { (_) in
//            alertViewController.dismiss(animated: true, completion: nil)
        }))
        
        present(alertViewController, animated: true, completion: nil)
    }
}
