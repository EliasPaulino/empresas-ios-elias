//
//  UIImage+imageOfType.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

extension UIImage {
    static func image(ofType imageType: ImageType) -> UIImage {
        guard let image = UIImage(named: imageType.rawValue) else {
            fatalError("the Image \(imageType.rawValue) couldnt be loaded from Assets")
        }
        
        return image
    }
}
