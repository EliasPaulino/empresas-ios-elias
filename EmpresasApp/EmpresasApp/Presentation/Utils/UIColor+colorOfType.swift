//
//  UIColor+colorOfType.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import UIKit

extension UIColor {
    static func color(ofType colorType: ColorType) -> UIColor {
        guard let color = UIColor(named: colorType.rawValue) else {
            fatalError("the Color \(colorType.rawValue) couldnt be loaded from Assets")
        }
        
        return color
    }
}

