//
//  UserLoginUserCase.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

protocol UserLoginUserCase {
    func login(user: User, completion: @escaping UserCaseActionCompletion<Error>)
}
