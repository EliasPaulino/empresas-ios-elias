//
//  LoggedUserUseCase.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

protocol LoggedUserUseCase {
    func getIsLoggedUser(completion: @escaping UserCaseCompletion<Bool, Error>)
}
