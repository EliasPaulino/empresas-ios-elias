//
//  GetEnterprisesUserCase.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

protocol GetEnterprisesUserCase {
    @discardableResult
    func getEnterprises(withType: EnterpriseType?, andQueryName: String?, completion: @escaping UserCaseCompletion<[Enterprise], Error>) -> CancelCompletion
}
