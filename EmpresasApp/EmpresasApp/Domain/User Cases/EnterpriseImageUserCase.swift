//
//  EnterpriseImageUserCase.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

protocol EnterpriseImageUserCase {
    func getImage(fromURL: URL, completion: @escaping UserCaseCompletion<Data, Error>) -> CancelCompletion
}
