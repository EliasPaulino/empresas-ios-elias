//
//  Enterprise.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct Enterprise: Codable {
    var enterpriseName: String
    var enterpriseType: EnterpriseType
    var photo: URL?
    var description: String
}
