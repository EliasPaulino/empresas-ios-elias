//
//  EnterpriseType.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

struct EnterpriseType: Codable {
    var id: Int
    var enterpriseTypeName: String?
    
    init(id: Int, name: String? = nil) {
        self.id = id
        self.enterpriseTypeName = name
    }
}
