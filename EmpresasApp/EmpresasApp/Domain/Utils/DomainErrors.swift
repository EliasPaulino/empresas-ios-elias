//
//  DomainErrors.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 22/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

enum DomainErrors: Error {
    case userCredentialsAreInvalid
}
