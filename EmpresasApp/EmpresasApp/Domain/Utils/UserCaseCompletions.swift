//
//  UserCaseCompletion.swift
//  EmpresasApp
//
//  Created by Elias Paulino on 21/03/20.
//  Copyright © 2020 Elias Paulino. All rights reserved.
//

import Foundation

typealias UserCaseCompletion<ReturnType, ErrorType: Error> = (Result<ReturnType, ErrorType>) -> Void
typealias UserCaseActionCompletion<ErrorType: Error> = (Result<Bool, ErrorType>) -> Void
